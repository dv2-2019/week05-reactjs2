export const gpaListFromServer = [
    {
        semester: 1,
        year: "2560",
        enrolledCourses: [
            {
                courseNo: "001101",
                courseName: "FUNDAMENTAL ENGLISH 1",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "206113",
                courseName: "CAL FOR SOFTWARE ENGINEERING",
                credit: "3.00",
                gpa: "C"
            },
            {
                courseNo: "751100",
                courseName: "ECONOMICS FOR EVERYDAY LIFE",
                credit: "3.00",
                gpa: "W"
            },
            {
                courseNo: "951100",
                courseName: "MODERN LIFE AND ANIMATION",
                credit: "3.00",
                gpa: "A"
            },
            {
                courseNo: "953103",
                courseName: "PROGRAMMING LOGICAL THINKING",
                credit: "2.00",
                gpa: "C+"
            },
            {
                courseNo: "953211",
                courseName: "COMPUTER ORGANIZATION",
                credit: "3.00",
                gpa: "B"
            }
        ],

        gpas: [
            {
                record: "ผลการศึกษา / Record",
                ca: "หน่วยกิจที่ลง / CA",
                ce: "หน่วยกิจที่ได้ / CE",
                sumGpa: "เกรดเฉลี่ย / GPA",
                avgGpa: "GPA รวม"
            },
            {
                record: "ภาคการศึกษานี้ / Semester 1/2560",
                ca: "17.00",
                ce: "14.00",
                sumGpa: "2.82",
                avgGpa: "2.82"
            }
        ]


    },
    {
        semester: 2,
        year: "2560",
        enrolledCourses: [
            {
                courseNo: "001102",
                courseName: "FUNDAMENTAL ENGLISH 2",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "011251",
                courseName: "LOGIC",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "953102",
                courseName: "ADT & PROBLEM SOLVING",
                credit: "3.00",
                gpa: "B"
            },
            {
                courseNo: "953104",
                courseName: "WEB UI DESIGN & DEVELOP",
                credit: "2.00",
                gpa: "B"
            },
            {
                courseNo: "953202",
                courseName: "INTRODUCTION TO SE",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "953231",
                courseName: "OBJECT ORIENTED PROGRAMMING",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "955100",
                courseName: "LEARNING THROUGH ACTIVITIES 1",
                credit: "1.00",
                gpa: "A"
            }
        ],
        gpas: [
            {
                record: "ผลการศึกษา / Record",
                ca: "หน่วยกิจที่ลง / CA",
                ce: "หน่วยกิจที่ได้ / CE",
                sumGpa: "เกรดเฉลี่ย / GPA",
                avgGpa: "GPA รวม"
            },
            {
                record: "ภาคการศึกษานี้ / Semester 1/2560",
                ca: "18.00",
                ce: "18.00",
                sumGpa: "2.72",
                avgGpa: "2.72"
            }
        ]

    }, {
        semester: 1,
        year: "2561",
        enrolledCourses: [
            {
                courseNo: "001201",
                courseName: "CRIT READ AND EFFEC WRITE",
                credit: "3.00",
                gpa: "D+"
            },
            {
                courseNo: "013110",
                courseName: "PSYCHOLOGY AND DAILY LIFE",
                credit: "3.00",
                gpa: "B+"
            },
            {
                courseNo: "206281",
                courseName: "DISCRETE MATHEMATICS",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "953212",
                courseName: "DB SYS & DB SYS DESIGN",
                credit: "3.00",
                gpa: "B"
            },
            {
                courseNo: "953233",
                courseName: "PROGRAMMING METHODOLOGY",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "953261",
                courseName: "INTERACTIVE WEB DEVELOPMENT",
                credit: "2.00",
                gpa: "C+"
            },
            {
                courseNo: "953361	",
                courseName: "COMP NETWORK & PROTOCOLS",
                credit: "3.00",
                gpa: "D"
            }

        ],
        gpas: [
            {
                record: "ผลการศึกษา / Record",
                ca: "หน่วยกิจที่ลง / CA",
                ce: "หน่วยกิจที่ได้ / CE",
                sumGpa: "เกรดเฉลี่ย / GPA",
                avgGpa: "GPA รวม"
            },
            {
                record: "ภาคการศึกษานี้ / Semester 1/2560",
                ca: "20.00",
                ce: "20.00",
                sumGpa: "2.35",
                avgGpa: "2.61"
            }
        ]

    },

    {
        semester: 2,
        year: "2561",
        enrolledCourses: [
            {
                courseNo: "001225",
                courseName: "ENGL IN SCIENCE & TECH CONT",
                credit: "3.00",
                gpa: "C+"
            },
            {
                courseNo: "206255",
                courseName: "MATH FOR SOFTWARE TECH",
                credit: "3.00",
                gpa: "B+"
            },
            {
                courseNo: "953201",
                courseName: "ALGO DESIGN & ANALYSIS",
                credit: "3.00",
                gpa: "C"
            },
            {
                courseNo: "953214",
                courseName: "OS & PROG LANG PRINCIPLES",
                credit: "3.00",
                gpa: "B+"
            },
            {
                courseNo: "953232",
                courseName: "OO ANALYSIS & DESIGN",
                credit: "3.00",
                gpa: "B"
            },
            {
                courseNo: "953234",
                courseName: "ADVANCED SOFTWARE DEVELOPMENT",
                credit: "2.00",
                gpa: "B+"
            },
            {
                courseNo: "955200",
                courseName: "LEARNING THROUGH ACTIVITIES 2",
                credit: "1.00",
                gpa: "A"
            }

        ],
        gpas: [
            {
                record: "ผลการศึกษา / Record",
                ca: "หน่วยกิจที่ลง / CA",
                ce: "หน่วยกิจที่ได้ / CE",
                sumGpa: "เกรดเฉลี่ย / GPA",
                avgGpa: "GPA รวม"
            },
            {
                record: "ภาคการศึกษานี้ / Semester 1/2560",
                ca: "19.00",
                ce: "19.00",
                sumGpa: "3.05",
                avgGpa: "2.73"
            }
        ]

    },
    {
        semester: 1,
        year: "2562",
        enrolledCourses: [
            {
                courseNo: "011151",
                courseName: "REASONING",
                credit: "3.00",
                gpa: "A"
            },
            {
                courseNo: "208263",
                courseName: "ELEMENTARY STATISTICS",
                credit: "3.00",
                gpa: "D+"
            },
            {
                courseNo: "259109",
                courseName: "TELECOM IN THAILAND",
                credit: "3.00",
                gpa: "A"
            },
            {
                courseNo: "953321",
                courseName: "SOFTWARE REQ ANALYSIS",
                credit: "3.00",
                gpa: "B"
            },
            {
                courseNo: "953332",
                courseName: "SOFTWARE DESIGN & ARCH",
                credit: "3.00",
                gpa: "C"
            },
            {
                courseNo: "953331	",
                courseName: "COMPO-BASED SOFTWARE DEV",
                credit: "2.00",
                gpa: "B"
            }

        ],
        gpas: [
            {
                record: "ผลการศึกษา / Record",
                ca: "หน่วยกิจที่ลง / CA",
                ce: "หน่วยกิจที่ได้ / CE",
                sumGpa: "เกรดเฉลี่ย / GPA",
                avgGpa: "GPA รวม"
            },
            {
                record: "ภาคการศึกษานี้ / Semester 1/2560",
                ca: "18.00",
                ce: "18.00",
                sumGpa: "2.92",
                avgGpa: "2.76"
            },
            {
                record: "สะสมทั้งหมด / Cumulative",
                ca: "",
                ce: "89.00",
                sumGpa: "2.76",
                avgGpa: ""
            }
        ]

    }





]
