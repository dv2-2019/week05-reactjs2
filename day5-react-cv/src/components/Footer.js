import React from 'react';
import '../About.css';
const Footer = () => {
  return (
    <div>
      {/* <!-- Footer --> */}
      <footer className="bg-black">
        <div className="container">
          <div className="social d-flex justify-content-center">
            <ul className="social-network social-circle">
              <li><a href="https://www.facebook.com/meenizzchanikan" className="icoFacebook" title="Facebook"><i
                className="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/zonzon_meen" className="icoTwitter" title="Twitter"><i
                className="fa fa-twitter"></i></a></li>
              <li><a href="mailto: zonzon.meen@gmail.com" className="icoGoogle" title="Google +"><i
                className="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.instagram.com/meen_izz/?hl=th" className="icoInstagram" title="Google +"><i
                className="fa fa-instagram"></i></a></li>
            </ul>

          </div>
        </div>
      </footer>
    </div>
  )
}

export default Footer;