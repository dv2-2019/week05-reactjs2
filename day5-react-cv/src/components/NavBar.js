import React from 'react';
import '../About.css';
const NavBar = () => {
  return (
    <div>

      {/* <!-- Navigation --> */}
      <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div className="container">
          <a href="https://www.camt.cmu.ac.th/" target="_blank" className="nav-link js-scroll-trigger" style={{ fontSize: "22px" }}>
            <img src="assets/icon.png" width="50" height="50" />
            CAMT CMU
            </a>

          <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
            Menu
        <i className="fas fa-bars"></i>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/">About Me</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/anime">Anime</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/academicrecord">Academic Record</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  )
}
export default NavBar;