import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter } from 'react-router-dom';
import AboutPage from './pages/AboutPage';
import AnimePage from './pages/AnimePage';
import GpaTable from './pages/GpaTable';

const MainRouting =
    <BrowserRouter>
        <Route path="/" component={AboutPage} exact></Route>
        <Route path="/anime" component={AnimePage}></Route>
        <Route path="/academicrecord" component={GpaTable}></Route>
    </BrowserRouter>
ReactDOM.render(MainRouting, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
