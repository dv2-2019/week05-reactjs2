import React from 'react';
import '../About.css';
import { FaMusic, FaBook, FaGamepad, FaPalette, FaHeadset } from "react-icons/fa";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Carousel } from 'react-bootstrap';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

const AboutPage = () => {

    return (
        <div>
            <NavBar />
            {/* <!-- Header --> */}
            <header className="masthead">
                <div className="container d-flex h-100 align-items-center">
                    <div className="mx-auto text-center">
                        <section className="content-section bg-light" style={{ marginTop: 80 }}>
                            {/* <!-- Featured Project Row --> */}
                            <div className="row align-items-center ">

                                <div className="col-xl-5 col-lg-5">
                                    <div className="featured-text text-center text-lg-left">
                                        <p>This is my profile</p>
                                        <h3>CHANIKAN CHAMRAT</h3>
                                        <p className="text-black-50 mb-0">Solftware Engineering,
                  College of Arts, Media and Technology, Chiangmai University</p> &nbsp;
                                        </div>

                                    <a href="#section02" className="btn btn-primary " >wellcome!!</a>
                                </div>
                                <div className="col-xl-7 col-lg-7">

                                    <Carousel style={{ paddingLeft: "8em" }}>
                                        <Carousel.Item style={{ width: "400px" }}>
                                            <img
                                                className="d-block w-100"
                                                src="/assets/profile.jpg"
                                                alt="First slide"

                                            />
                                        </Carousel.Item>
                                        <Carousel.Item style={{ width: "400px" }}>
                                            <img
                                                className="d-block w-100"
                                                src="/assets/profile1.jpg"
                                                alt="Third slide"
                                            />

                                        </Carousel.Item>
                                        <Carousel.Item style={{ width: "400px" }}>
                                            <img
                                                className="d-block w-100"
                                                src="/assets/profile2.jpg"
                                                alt="Third slide"
                                            />

                                        </Carousel.Item>
                                        <Carousel.Item style={{ width: "400px" }}>
                                            <img
                                                className="d-block w-100"
                                                src="/assets/profile3.jpg"
                                                alt="Fourth slide"
                                            />
                                        </Carousel.Item>
                                    </Carousel>

                                </div>
                            </div>

                        </section>
                    </div>
                </div>
            </header>


            {/* <!-- About Section --> */}
            <section className="about-section text-center" id="section02" >
                <div className="container">
                    <div className="row">
                        <section className="content-section bg-light" style={{ marginBottom: " 50px" }} >
                            <div className="row">
                                <div className="col-5 text-lg-left" >
                                    <h3>ABOUT ME </h3> &nbsp;
                                        <p>NAME : Chanikan Chamrat</p>
                                    <p>NICKNAME : Meen</p>
                                    <p>BIRTH : 19 February 1998</p>
                                    <p>AGE : 21 years old</p>
                                    <p>ADDRESS : 133 Nongwaen No.9, Maechan, Chiang Rai</p>
                                    <p>PREFERENCE :</p>
                                    <ul>
                                        <li>Dancing <FaMusic /></li>
                                        <li>Novel <FaBook /></li>
                                        <li>Game <FaGamepad /> <FaHeadset /></li>
                                        <li>Painting <FaPalette /></li>
                                    </ul>
                                    <p>CONTACT : </p>
                                    <ul className="social-network social-circle" id="about">
                                        <li><a href="https://www.facebook.com/meenizzchanikan" className="icoFacebook" title="Facebook"><i
                                            className="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/zonzon_meen" className="icoTwitter" title="Twitter"><i
                                            className="fa fa-twitter"></i></a></li>
                                        <li><a href="mailto: zonzon.meen@gmail.com" className="icoGoogle" title="Google +"><i
                                            className="fa fa-google-plus"></i></a></li>
                                        <li><a href="https://www.instagram.com/meen_izz/?hl=th" className="icoInstagram" title="Google +"><i
                                            className="fa fa-instagram"></i></a></li>
                                    </ul>
                                </div>
                                <div className="col-7">
                                    <div className="featured-text text-center text-lg-left">
                                        <p>TECHNICAL SKILLS :</p>
                                        <ol>
                                            <li> Programming language</li>
                                            &nbsp;
                                            <div className="row">
                                                <div className="col">
                                                    <img src="https://image.flaticon.com/icons/svg/919/919828.svg" style={{ width: "80px" }}></img>
                                                </div>
                                                <div className="col">
                                                    <img src="https://c.s-microsoft.com/en-ie/CMSImages/tools-for-java-1.png?version=4bd8ce88-1f2d-4890-c7b3-b1218e0fde2e" style={{ width: "200px" }}></img>
                                                </div>
                                            </div>
                                            <li>Others</li>
                                            &nbsp;
                                                <div className="row">
                                                <div className="col">
                                                    <img src="https://image.flaticon.com/icons/svg/919/919827.svg" style={{ width: "80px" }}></img>
                                                </div>
                                                <div className="col">
                                                    <img src="https://image.flaticon.com/icons/svg/919/919826.svg" style={{ width: "80px" }}></img>
                                                </div>
                                                <div className="col">
                                                    <img src="https://pluspng.com/img-png/bootstrap-png-bootstrap-512.png" style={{ width: "100px" }}></img>
                                                </div>
                                                <div className="col">
                                                    <img src="https://teamextension.jp/dist/img/skills/angularjs_og.png" style={{ width: "80px" }}></img>
                                                </div>
                                            </div>
                                        </ol>
                                        <p>LANGUAGES : </p>
                                        <div className="row">
                                            <div className="col">
                                                <img src="https://image.flaticon.com/icons/svg/330/330447.svg" style={{ width: "50px" }}></img>
                                                &nbsp; Thai : Good
                                                </div>
                                            <div className="col">
                                                <img src="https://image.flaticon.com/icons/svg/330/330459.svg" style={{ width: "50px" }}></img>
                                                &nbsp; English : Good
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>

                </div>
            </section>

            {/* <!-- content Section --> */}
            <section className="content-section bg-light" >
                <div className="container">
                    {/* <!-- Project One Row --> */}
                    <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
                        <div className="col-lg-6">
                            <img className="img-fluid" src="assets/ac1.jpg" alt=""></img>
                        </div>
                        <div className="col-lg-6">
                            <div className="bg-black text-center h-100 project">
                                <div className="d-flex h-100">
                                    <div className="project-text w-100 my-auto text-center text-lg-left">
                                        <h4 className="text-white">CAMT FEST</h4>
                                        <p className="mb-0 text-white-50">กิจกรรมครบรอบ 15 ปี CAMT วิ่งแข่งจับมอนสเตอร์โดยใช้แอพลิเคชั่นเกมในการเล่น
                                          พร้อมกับวิ่งรอบมหาวิทยาลัยเชียงใหม่
                  ที่ฉันอยู่ตอนนี้คือภายในงานจะมีการจัดบูทกิจกรรมเกมต่างๆให้เล่น</p>
                                        <hr className="d-none d-lg-block mb-0 ml-0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <!-- Project Two Row --> */}
                    <div className="row justify-content-center no-gutters">
                        <div className="col-lg-6">
                            <img className="img-fluid" src="assets/ac2.jpeg" alt=""></img>
                        </div>
                        <div className="col-lg-6 order-lg-first">
                            <div className="bg-black text-center h-100 project">
                                <div className="d-flex h-100">
                                    <div className="project-text w-100 my-auto text-center text-lg-right">
                                        <h4 className="text-white">KANTOK DAY</h4>
                                        <p className="mb-0 text-white-50">กิจกรรมร่วมรับประทานอาหารเย็นกับเพื่อนๆ พร้อมการแสดงที่หลากหลาย
                  โดยกินข้าวจากขันโตกตามประเพณีของภาคเหนือ</p>
                                        <hr className="d-none d-lg-block mb-0 mr-0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <!-- Project Three Row --> */}
                    <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
                        <div className="col-lg-6">
                            <img className="img-fluid" src="assets/ac3.jpg" alt=""></img>
                        </div>
                        <div className="col-lg-6">
                            <div className="bg-black text-center h-100 project">
                                <div className="d-flex h-100">
                                    <div className="project-text w-100 my-auto text-center text-lg-left">
                                        <h4 className="text-white">DOI SUTHEP TREKKING</h4>
                                        <p className="mb-0 text-white-50">กิจกรรมรับน้องขึ้นดอย ตามประเพณีของมหาวิทยาลัยเชียงใหม่
                  ตอนนี้ฉันกำลังอยู่ในจุดเริ่มต้นพร้อมเพื่อนๆ เพื่อที่จะขึ้นดอยด้วยกันในตอนเช้า</p>
                                        <hr className="d-none d-lg-block mb-0 ml-0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {/* <!-- Project Four Row --> */}
                    <div className="row justify-content-center no-gutters">
                        <div className="col-lg-6">
                            <img className="img-fluid" src="assets/ac4.jpg" alt=""></img>
                        </div>
                        <div className="col-lg-6 order-lg-first">
                            <div className="bg-black text-center h-100 project">
                                <div className="d-flex h-100">
                                    <div className="project-text w-100 my-auto text-center text-lg-right">
                                        <h4 className="text-white">GRADUATED DAY</h4>
                                        <p className="mb-0 text-white-50">รูปถ่ายรวมกับเพื่อนๆชั้น ม.6/1 และคุณครู ในวันจบการศึกษาในระดับมัธยมศึกษา
                  2559 ที่โรงเรียนแม่จันวิทยาคม จังหวัดเชียงราย </p>
                                        <hr className="d-none d-lg-block mb-0 mr-0" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <Footer />
        </div>
    )
}

export default AboutPage;