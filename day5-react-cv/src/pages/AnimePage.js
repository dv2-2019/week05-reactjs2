import React, { useState, useEffect } from 'react';
import '../About.css';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import { Card } from 'antd';
import { Input } from 'antd';
import { Select, List } from 'antd';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

const { Option } = Select;
const { Search } = Input;
const AnimePage = () => {
    const [animeList, setAnimeList] = useState([]);
    const [search, setSearch] = useState('one piece');
    const [selected, setSelected] = useState(-1);
    const [typeList, setTypeList] = useState([]);

    const [animeFilterType, setAnimeFilterType] = useState([]);

    useEffect(() => {
        console.log('useEffect')
        fetchAnimeData();
    }, [search, selected])

    const fetchAnimeData = () => {
        console.log('fetchAnimeData')
        fetch('https://api.jikan.moe/v3/search/anime?q=' + search)
            .then(response => response.json())
            .then(data => {

                var tmpTypeList = [];
                for (var i = 0; i < data.results.length; i++) {
                    var item = data.results[i];
                    if (!tmpTypeList.find(m => m == item.type)) {
                        tmpTypeList.push(item.type)
                    }
                }

                setTypeList(tmpTypeList)
                setAnimeList(data.results);
                console.log('List All data', data.results);
            })
            .catch(error => console.log(error));
    }

    const onSearchAnime = event => {
        console.log(event.target.value)
        setSearch(event.target.value)
    }

    const onSelected = value => {
        setSelected(value)
        console.log(value);
    }
    console.log('searched', animeList)
    console.log('search', search)
    console.log('types', typeList)

    let renderItems = animeList;
    renderItems = selected == -1 ?
        renderItems
        :
        renderItems.filter(data => data.type.includes(typeList[selected]));
    console.log(renderItems)

    return (
        <div>
            <NavBar />
            {/* <!-- Search Section --> */}
            <section className="search-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-10 col-lg-8 mx-auto text-center">
                            <h2 className="text-white mb-5">My favorite Anime</h2>

                            <Search
                                placeholder="Please input anime name..."
                                onSearch={value => console.log(value)}
                                onChange={onSearchAnime}
                                style={{ width: 500, height: 35, margin: 10 }}
                            />
                            <Select defaultValue="All"
                                style={{ width: 120 }} onChange={onSelected}>
                                <Option value={-1}>All</Option>
                                {
                                    typeList.map((data, index) => {
                                        return (
                                            <Option value={index}>{data}</Option>
                                        )
                                    })
                                }
                            </Select>
                        </div>
                    </div>
                </div>

            </section>

            {/* <!-- Anime List Section --> */}
            <section className="content-section bg-light">
                <div className="container">
                    {/* <!-- Anime Row --> */}
                </div>
                <div className="container">
                    <List
                        dataSource={
                            renderItems
                        }
                        renderItem={(item) => (

                            <Card hoverable style={{ marginBottom: "2em" }} >
                                <div className="row" >
                                    <div className="col-md-4">
                                        <img src={item.image_url} className="w-100" style={{ width: "300px", height: "250px" }} />
                                    </div>
                                    <div className="col-md-8" style={{ paddingTop: "2em", paddingRight: "1em" }}>
                                        <h4> {item.title}</h4>
                                        <p> {item.synopsis}</p>
                                        <p> Episodes : {item.episodes}</p>
                                        <p> Type : {item.type}</p>
                                        <p>Score : {item.score}</p>
                                    </div>
                                </div>
                            </Card>

                        )} />


                </div>
            </section>

            <Footer />
        </div >
    )

}
export default AnimePage;