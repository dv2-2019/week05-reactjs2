import React from 'react';
import '../About.css';

import 'bootstrap/dist/css/bootstrap.min.css';
const AcademicRecord = (props) => {

    return (
        <div>&nbsp;
            <div className="col-md-3">
                <h4 >Semester {props.displayGpaList.semester} / {props.displayGpaList.year}</h4>
            </div>


            <table className="table table-bordered table-hover">
                <thead className="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Course No</th>
                        <th scope="col">Course Name</th>
                        <th scope="col">Credit</th>
                        <th scope="col">Grade</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.displayGpaList.enrolledCourses.map((displayGpaList, index) => {
                            return (
                                <tr className="table-light">
                                    <td>{index + 1}</td>
                                    <td>{displayGpaList.courseNo}</td>
                                    <td>{displayGpaList.courseName}</td>
                                    <td>{displayGpaList.credit}</td>
                                    <td>{displayGpaList.gpa}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>

            <table className="table table-bordered ">
                <thead className="table-secondary">
                    <tr>
                        <th scope="col">ผลการศึกษา / Record</th>
                        <th scope="col">หน่วยกิจที่ลง / CA</th>
                        <th scope="col">หน่วยกิจที่ได้ / CE</th>
                        <th scope="col">เกรดเฉลี่ย / GPA</th>
                        <th scope="col">GPA รวม</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.displayGpaList.gpas.map((gpas) => {
                            return (
                                <tr className="table-light">
                                    <td>{gpas.record}</td>
                                    <td>{gpas.ca}</td>
                                    <td>{gpas.ce}</td>
                                    <td>{gpas.sumGpa}</td>
                                    <td>{gpas.avgGpa}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>

        </div>
    )
}
export default AcademicRecord;