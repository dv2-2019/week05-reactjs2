import React, { useState, useEffect } from 'react';
import AcademicRecord from './AcademicRecord'
import { gpaListFromServer } from '../data/GpaData'
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';

const GpaTable = () => {

    const arr = ['a0', 'a1', 'a2', 'a3', 'a4'];

    console.log(arr[3]);
    arr.map((data, index) => {
        // console.log(data);
    })
    const [gpas, setGpas] = useState([]);
    const [selectedIndex, setSelectedIndex] = useState(-1);


    useEffect(() => {
        setGpas(gpaListFromServer);

    }, [])

    const onSelectChange = (event) => {
        setSelectedIndex(event.target.value)
    }
    return (

        <div>
            <NavBar />


            {/* <!-- Gpa Section --> */}
            <section className="headGpa-section">
                <div className="container">
                    <div className="row">
                        <div className="col mx-auto text-center">
                            <h2 className=" text-white">My academic record</h2>


                        </div>
                    </div>
                </div>
            </section>
            <section className="gpaTable">
                <div className="container">
                    <div className="row">
                        <div className="col mx-auto text-center">
                            {/* <!-- Filter gpa --> */}
                            <form>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon3">Search GPA</span>
                                            </div>
                                            <select id="inputState" class="form-control" onChange={onSelectChange
                                            }>
                                                <option value={-1} >All</option>
                                                {
                                                    gpas.map((data, index) => {
                                                        return (
                                                            <option value={index}>{data.semester}/{data.year}</option>
                                                        )
                                                    })
                                                }
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            {
                                selectedIndex == -1 ?
                                    <div>
                                        {
                                            gpas.map((data) => {
                                                return (
                                                    <AcademicRecord displayGpaList={data} />
                                                )
                                            })
                                        }
                                    </div>
                                    :
                                    <AcademicRecord displayGpaList={gpas[selectedIndex]} />
                            }
                        </div>
                    </div>

                </div>
            </section>
            <Footer />
        </div>
    );
}
export default GpaTable;



