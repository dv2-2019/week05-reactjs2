import React from 'react'
class UserPage extends React.Component {
    constructor() {
        super();
        this.state = {
            users: []
        }
    }
    //component Life cycle
    componentDidMount() {
        this.fetchUser();
    }
    fetchUser() {
        fetch('http://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    users: data
                })
            })
            .catch(error => console.log(error));
    }


    render() {
        var { users } = this.state;
        return (
            <div>
                <div> User Page </div>
                <div>
                    {
                        users.map((item, index) => {
                            return (
                                <div key={index}>Hello
                                <p>ID : {item.id}</p>
                                    <p>Name : {item.name}</p>
                                    <p>User Name : {item.username}</p>
                                    <p>Email : {item.email}</p>
                                    <hr></hr>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default UserPage;