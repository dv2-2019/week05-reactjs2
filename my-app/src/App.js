import React from 'react';
import logo from './logo.svg';
import './App.css';
import UserPage from './pages/UserPage';
import HomePage from './pages/HomePage';


function App() {
  return (
    <div>
      <UserPage />
      <HomePage></HomePage>
      {/* <Header ></Header> */}
    </div>

  );

}

export default App;
