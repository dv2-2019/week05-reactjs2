import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const Display = (props) => {
    const personLists = props.personList;
    // const { personList } = props;
    // handleClick = () => {
    //     this.setState({
    //         buttonState: !buttonState
    //     })
    // }

    return (
        <table className="table">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Todo</th>
                    <th scope="col"></th>
                </tr>
            </thead>

            <tbody>
                {personLists.map((list, index) => {
                    return (
                        <tr>
                            <td>{index + 1}</td>
                            <td>
                                {
                                    list.isDone ?
                                        <div style={{ textDecorationLine: 'line-through' }} >{list.name}</div>
                                        :
                                        <div >{list.name}</div>
                                }
                            </td>
                            <td>
                                {
                                    list.isDone ?
                                        <button className="btn btn-danger" type="button" onClick={() => props.removeTodo(index)} >Remove</button>
                                        :
                                        <button className="btn btn-secondary" type="button" onClick={() => props.doneTodo(index)} >Done</button>
                                }
                            </td>
                        </tr>
                    )
                })
                }
            </tbody>
        </table>


    )
}
export default Display;