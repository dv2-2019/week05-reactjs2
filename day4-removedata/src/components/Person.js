import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
class Person extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            isDone : false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
 
    handleChange(event) {
        return function (e) {
            var state = {};
            state[event] = e.target.value;
            this.setState(state);
        }.bind(this);
    }

    handleSubmit(event) {
        var data = {
            name: this.state.name,
            isDone: this.state.isDone
        }
        this.props.onAddPerson(data);
        event.preventDefault();

        //clear submit form
        const name = this.state.name;
        this.setState({
            name: '',
        });
    }

render(){
return (
    <form onSubmit={this.handleSubmit}>
        <div className="row">
            <div className="col">
                <div className="input-group mb-3">
                    <input type="text" className="form-control" placeholder="name" aria-describedby="button-addon1" value={this.state.name} onChange={this.handleChange('name')}  />
                    <div className="input-group-append">
                        <button className="btn btn-secondary" type="submit" id="button-addon2" >ADD</button>
                    </div>
                </div>

            </div>

        </div>

    </form>
)    
}}
    export default Person; 