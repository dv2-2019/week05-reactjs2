import React from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './components/Person'
import Display from './components/Display'

class App extends React.Component {
  state = {
    list: [],
  }
  displayInfo = data => {
    const newList = [...this.state.list, data];
    this.setState({ list: newList });
    console.log(newList)
  }

  doneTodo = (index) => {
    this.state.list[index].isDone = true
    this.setState({
      list: this.state.list
    })
  }
  removeTodo = (index) => {
    this.state.list[index].isDone = false
    this.state.list.splice(index,1);
    this.setState({
      list: this.state.list
    }) 

  }
  render() {
    return (
      <div className="container" >
        <Person onAddPerson={this.displayInfo} />
        <Display personList={this.state.list}
          doneTodo={this.doneTodo}
          removeTodo={this.removeTodo}
        />
      </div>
    );
  }
}
export default App;
