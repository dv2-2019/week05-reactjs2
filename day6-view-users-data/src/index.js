import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import UsersPage from './pages/UsersPage';
import TodoPage from './pages/TodoPage';
import UserPhoto from './pages/UserPhoto';
import UserAlbums from './pages/UserAlbums';

import { Route, BrowserRouter } from 'react-router-dom';

const MainRouting =
    <BrowserRouter>
        <Route path="/users" component={UsersPage} exact={true}></Route>
        <Route path="/users/:user_id/todo" component={TodoPage}></Route>
        <Route path="/users/:user_id/albums" component={UserAlbums} exact={true}></Route>
        <Route path="/users/:user_id/albums/:album_id/photos" component={UserPhoto}></Route>
    </BrowserRouter>
ReactDOM.render(MainRouting , document.getElementById('root'));

serviceWorker.unregister();
