import React, { useState, useEffect } from 'react';
import { List, Avatar, Button, Skeleton } from 'antd';
import 'antd/dist/antd.css'
import { Layout, Menu, Row, Card, Input, Icon } from 'antd';

const { Header } = Layout;
const { Search } = Input;
const UsersPage = (props) => {

    const [data, setData] = useState([]);
    const [search, setSearch] = useState('');

    useEffect(() => {
        console.log('useEffect')
        fetchUserDataFromServer();
    }, [])

    const fetchUserDataFromServer = () => {
        console.log('fetchAnimeData')
        fetch('http://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                setData(data)
                console.log(data)
            })
            .catch(error => console.log(error));
    }

    const onSearchUser = event => {
        console.log(event.target.value)
        setSearch(event.target.value)
    }

    return (
        <Row style={{ backgroundColor: '#e6e6e6' }}>

            <Header className="header">

                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['2']}
                    style={{ lineHeight: '64px' }}
                    title=" User">
                    <Icon type="user" style={{ fontSize: '22px', color: '#ffff' }} />
                </Menu>
            </Header>
            <Card title="User Page" hoverable style={{ padding: '1em', margin: '4em' }} >
                <Row>
                    <Search
                        placeholder="Please input user name..."
                        onSearch={value => console.log(value)}
                        style={{ width: 300, marginBottom: '2em' }} onChange={onSearchUser} />
                </Row>


                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={
                        search ?
                            data.filter(m => m.name.match(search))
                            :
                            data
                    }
                    renderItem={item => (
                        <List.Item
                            actions={[<a href={"/users/" + item.id + "/todo"}>Todo</a>, <a href={"/users/" + item.id + "/albums"}>Albums</a>]}>
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://img.icons8.com/bubbles/2x/system-administrator-female.png" />}
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={item.email} />
                                <div>{item.phone} | {item.website}</div>
                            </Skeleton>

                        </List.Item>
                    )}
                />
            </Card>
        </Row>
    )
}
export default UsersPage;