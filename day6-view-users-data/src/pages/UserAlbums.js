import React, { useState, useEffect } from 'react';
import { List, Typography, Row, PageHeader, Card } from 'antd';

const UserAlbums = (props) => {

    const [albumsList, setAlbumsList] = useState([]);
    const [selectedAlbums, setSelectedAlbums] = useState([]);

    useEffect(() => {
        console.log('useEffect')
        fetchSelectAlbums();
        fetchAlbumsList();
    }, [])

    // display all 
    const fetchAlbumsList = () => {
        const userId = props.match.params.user_id;
        console.log('fetchAlbumsList')
        fetch('http://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbumsList(data)
                console.log(data)
            })
            .catch(error => console.log(error));
    }

    // display each album 
    const fetchSelectAlbums = () => {
        const albumId = props.match.params.album_id;
        console.log('fetchSelectAlbums')
        fetch('http://jsonplaceholder.typicode.com/albums?userId=' + albumId)
            .then(response => response.json())
            .then(data => {
                setSelectedAlbums(data)
                console.log(data)
            })
            .catch(error => console.log(error));
    }

    const userId = props.match.params.user_id;
    const albumId = props.match.params.album_id;
    console.log('album Id', albumId)

    return (
        <Row style={{ backgroundColor: '#e6e6e6' }}>
            <PageHeader
                ghost={false}
                onBack={() => window.history.back()}
                title="Back"
            ></PageHeader>
            <Card title="User Albums Listnpm start" hoverable style={{ margin: '4em' }}>
                <List
                    size="large"
                    header={<div>Albums</div>}
                    footer={<div>Footer</div>}
                    bordered
                    dataSource={albumsList}
                    renderItem={item =>
                        <List.Item>
                            <a href={"/users/" + userId + "/albums/" + item.id + "/photos"}>{item.title}</a>
                        </List.Item>}
                /></Card>
        </Row>
    )
}
export default UserAlbums; 